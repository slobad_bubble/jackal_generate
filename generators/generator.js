const stringG = () => "Joi.string().required()";
const numberG = () => "Joi.number().int().required()";
const booleanG = () => "Joi.boolean().required()";
const arrayG = (value) => [...new Set(value.map(x => recurse(x)))][0];
const objectG = (value) => Object.entries(value)
  .reduce((acc, curr) => {
    const [key, value] = curr;
    acc[key] = recurse(value);
    return acc;
  }, {});

const recurse = (value) => {
  const type = Array.isArray(value) ? "array" : typeof value;
  switch (type) {
    case "string":
      return stringG();
    case "number":
      return numberG();
    case "boolean":
      return booleanG();
    case "object":
      return objectG(value);
    case "array":
      return arrayG(value);
    default:
      return () => "<UNKNOWN TYPE>";
  }
};

module.exports = recurse;