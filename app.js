const data = require('./data/sample.1.json');
const generator = require('./generators/generator');
const util = require('util');

const result = Object.entries(data).reduce((acc, curr) => {
  const [key, value] = curr;
  acc.body[key] = generator(value);
  return acc;
}, { body: {} });

console.log(util.inspect(result, false, null))